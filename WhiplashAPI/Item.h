//
//  Item.h
//  WhiplashAPI
//
//  Created by Kevin Young on 4/20/15.
//  Copyright (c) 2015 Kevin Young. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject
@property (nonatomic, strong) NSString *sku;
@property (nonatomic, strong) NSString *itemDescription;
@property (nonatomic, strong) NSString *itemID;

+(instancetype)itemFromDictionary:(NSDictionary*)dict;
-(NSString*)stringToDisplay;

@end
