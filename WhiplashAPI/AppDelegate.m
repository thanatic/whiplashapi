//
//  AppDelegate.m
//  WhiplashAPI
//
//  Created by Kevin Young on 4/16/15.
//  Copyright (c) 2015 Kevin Young. All rights reserved.
//

#import "AppDelegate.h"

//#import "Whiplash.h"
#import "Item.h"

@interface AppDelegate ()
//@property (nonatomic, strong) Whiplash *whip;
@property (unsafe_unretained) IBOutlet NSTextField *resultField;
@property (nonatomic) int pageNumber;
@property (unsafe_unretained) IBOutlet NSTextField *pageNumberField;

@property (unsafe_unretained) IBOutlet NSWindow *window;
@end

@implementation AppDelegate
#pragma mark - standard AppDelegate stuff (init, teardown)
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    self.pageNumber = 1;
    [self updatePageNumber];
//    self.whip = [[Whiplash alloc] init];
//    self.whip.appdelegate = self;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}




#pragma mark - IBActions

- (IBAction)EnterPressed:(id)sender {
    if ([sender isKindOfClass:[NSTextField class]]) {
        NSTextField *textField = (NSTextField*)sender;
        [self infoFromItem:textField.stringValue];
    }
    
}


- (IBAction)pageBack:(id)sender {
    if (self.pageNumber > 1) {
        self.pageNumber -= 1;
        [self updatePageNumber];
    }
}

- (IBAction)pageForward:(id)sender {
    self.pageNumber += 1;
    [self updatePageNumber];
}

- (IBAction)pageEntered:(id)sender {
    if ([sender isKindOfClass:[NSTextField class]]) {
        NSTextField *textField = (NSTextField*)sender;
        self.pageNumber = [textField.stringValue intValue];
        [self updatePageNumber];
    }
}




#pragma mark - other
-(void)updatePageNumber {
    self.resultField.stringValue = [NSString stringWithFormat:@"loading Page %d", self.pageNumber];
    [self infoFromPage:self.pageNumber];
    self.pageNumberField.stringValue = [NSString stringWithFormat:@"%d", self.pageNumber];
}

-(void)addItem:(Item*)item {
    NSLog(@"adding");
    self.resultField.stringValue = [NSString stringWithFormat:@"%@, %@, %@", item.itemID, item.sku, item.itemDescription];
}

-(void)infoFromPage:(int)pageNumber {
    NSString *baseURL = @"http://testing.whiplashmerch.com/api/items.json?page=";
    NSString *requestString = [NSString stringWithFormat:@"%@%d", baseURL, pageNumber];
    
    
    NSURL *URL = [NSURL URLWithString:requestString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
    
    NSString *apiKey = @"Hc2BHTn3bcrwyPooyYTP";
    [request setValue:apiKey forHTTPHeaderField:@"X-API-KEY"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}

-(void)infoFromItem:(NSString *)idString {
    int itemID = (int)[idString integerValue];
    NSString *baseURL = @"http://testing.whiplashmerch.com/api/items/";
    NSString *requestString = [NSString stringWithFormat:@"%@%d", baseURL, itemID];
    
    
    NSURL *URL = [NSURL URLWithString:requestString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
    
    NSString *apiKey = @"Hc2BHTn3bcrwyPooyYTP";
    [request setValue:apiKey forHTTPHeaderField:@"X-API-KEY"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
    NSLog(@"connected");
}





#pragma mark - NSURLConnection Delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSError *error = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if (error != nil) {
        NSLog(@"Error parsing JSON.");
    } else {
        NSString *fullListString = @"";
        
        if ([jsonArray isKindOfClass:[NSDictionary class]]) {
            //1
            NSDictionary *itemData = (NSDictionary*)jsonArray;
            Item *item = [Item itemFromDictionary:itemData];
            [self addItem:item];
            
            fullListString = [NSString stringWithFormat:@"%@\n%@", fullListString, [item stringToDisplay]];
            
            
        } else if ([jsonArray isKindOfClass:[NSArray class]]) {
            //many
            NSLog(@"items to decode: %lu", (unsigned long)jsonArray.count);
            
            
            if (jsonArray.count == 0) {
                fullListString = [NSString stringWithFormat:@"no items found of page %d.\nPerhaps there are less than %d items in the database?", self.pageNumber, (self.pageNumber-1)*50];
            }
            for (id itemData in jsonArray) {
                if ([itemData isKindOfClass:[NSDictionary class]]) {
                    Item *item = [Item itemFromDictionary:itemData];
                    fullListString = [NSString stringWithFormat:@"%@\n%@", fullListString, [item stringToDisplay]];
                } else {
                    NSLog(@"NotDict: %@", [itemData class]);
                }
            }
        }
        self.resultField.stringValue = fullListString;
    }
    //    ...
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
//    NSLog(@"response: %@, %@", response, response.MIMEType);
    //    ...
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
//    NSLog(@"finished!");
    //    ...
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"error: %@", error);
    //    ...
}

@end
