//
//  main.m
//  WhiplashAPI
//
//  Created by Kevin Young on 4/16/15.
//  Copyright (c) 2015 Kevin Young. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
