//
//  Item.m
//  WhiplashAPI
//
//  Created by Kevin Young on 4/20/15.
//  Copyright (c) 2015 Kevin Young. All rights reserved.
//

#import "Item.h"

@implementation Item

+(instancetype)itemFromDictionary:(NSDictionary *)dict {
    Item *item = [[Item alloc] init];
//    NSLog(@"%@", dict);
//    for (id entry in [dict allKeys]) {
//        NSLog(@"", entry)
//    }
    
    
    item.sku = [dict valueForKey:@"sku"];
    item.itemID = [dict valueForKey:@"id"];
    item.itemDescription = [dict valueForKey:@"description"];
    
    return item;
}

-(NSString*)stringToDisplay {
    return [NSString stringWithFormat:@"(%@) - %@ -  \'%@\'", self.itemID, self.sku, self.itemDescription];
}


@end
