//
//  AppDelegate.h
//  WhiplashAPI
//
//  Created by Kevin Young on 4/16/15.
//  Copyright (c) 2015 Kevin Young. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class Item;


@interface AppDelegate : NSObject <NSApplicationDelegate>

-(void)addItem:(Item*)item;

@end

