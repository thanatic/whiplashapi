////
////  Whiplash.m
////  WhiplashAPI
////
////  Created by Kevin Young on 4/16/15.
////  Copyright (c) 2015 Kevin Young. All rights reserved.
////
//
//#import "Whiplash.h"
//
//#import "Item.h"
//
//@implementation Whiplash
//
//-(instancetype)init {
//    if (self = [super init]) {
////        [self setupStuff];
////        [self playOffline];
//    }
//    return self;
//}
//
//
//-(void)infoFromItem:(NSString *)idString {
//    int itemID = (int)[idString integerValue];
//    NSString *baseURL = @"http://testing.whiplashmerch.com/api/items/";
//    NSString *requestString = [NSString stringWithFormat:@"%@%d", baseURL, itemID];
//    
//    
//    NSURL *URL = [NSURL URLWithString:requestString];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
//    
//    NSString *apiKey = @"Hc2BHTn3bcrwyPooyYTP";
//    [request setValue:apiKey forHTTPHeaderField:@"X-API-KEY"];
//
//    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    [connection start];
//    NSLog(@"connected");
//}
//
//
//-(void)playOffline {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *rawData = [defaults objectForKey:@"RawData"];
//    
//    NSError *error = nil;
//    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:rawData options:kNilOptions error:&error];
//    
//    if (error != nil) {
//        NSLog(@"Error parsing JSON.");
//    }
//    else {
////        NSLog(@"Array: %@", jsonArray);
//        NSLog(@"%lu", (unsigned long)jsonArray.count);
//        
//        for (NSDictionary *itemData in jsonArray) {
//            if ([itemData isKindOfClass:[NSDictionary class]]) {
//                Item *item = [Item itemFromDictionary:itemData];
//                
//                NSLog(@"%@, %@, %@, %@", item.itemID, item.sku, item.itemDescription, [item valueForKey:@"quantity"]);
//                [self.appdelegate addItem:item];
//            } else {
//                NSLog(@"%@", [itemData class]);
//            }
//        }
//        
//    }
//    
//    
//    
//    
////    NSString *decodedString = [[NSString alloc] initWithData:rawData encoding:1];
////    NSLog(@"data:\n%@", decodedString);
//}
//
//
//-(void)setupStuff {
//    NSString *urlString = @"http://testing.whiplashmerch.com/api/items.json?page=1";
////    NSString *urlString = @"http://testing.whiplashmerch.com/api/items/141080";
//    
//    
//    // Setup NSURLConnection
//    NSURL *URL = [NSURL URLWithString:urlString];
////    NSURLRequest *request = [NSURLRequest requestWithURL:URL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
//    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:URL];
//    NSString *apiKey = @"Hc2BHTn3bcrwyPooyYTP";
//    
////    NSData *data = [apiKey dataUsingEncoding:NSUTF8StringEncoding];
////    apiKey = [data base64EncodedStringWithOptions:0];
//    
////    NSString *authValue = [NSString stringWithFormat:@"Basic %@", AFBase64EncodedStringFromString(basicAuthCredentials)];
//    [request setValue:apiKey forHTTPHeaderField:@"X-API-KEY"];
////    [request setValue:@"1" forHTTPHeaderField:@"X-API-VERSION"];
//    
////    NSURLConnection *connection = [NSURLConnection alloc] init
//
//    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
//    [connection start];
//    //[connection release];
//}
//
//
//
//// NSURLConnection Delegates
//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
//    NSLog(@"recieved challenge");
//    
//    
////    if ([challenge previousFailureCount] == 0) {
////        NSLog(@"received authentication challenge");
////        NSURLCredential *newCredential = [NSURLCredential credentialWithUser:@"USER"
////                                                                    password:@"PASSWORD"
////                                                                 persistence:NSURLCredentialPersistenceForSession];
////        NSLog(@"credential created");
////        [[challenge sender] useCredential:newCredential forAuthenticationChallenge:challenge];
////        NSLog(@"responded to authentication challenge");
////    }
////    else {
////        NSLog(@"previous authentication failure");
////    }
//}
//
//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
//    
//    NSLog(@"response");
////    NSLog(@"response: %@, %@", response, response.MIMEType);
////    ...
//}
//
//- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
////    NSString *result = [NSString stringWithFormat:@"Basic %@", [data base64EncodedStringWithOptions:0]];
////    NSLog(@"data: \n%@\n64:\n%@", data, result);
//    
////    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
////    [defaults setObject:data forKey:@"RawData"];
////    [defaults synchronize];
//    
////    NSString *decodedString = [[NSString alloc] initWithData:data encoding:1];
////    NSLog(@"data:\n%@", decodedString);
//    NSLog(@"data");
//    
//    
//    NSError *error = nil;
//    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//    
//    if (error != nil) {
//        NSLog(@"Error parsing JSON.");
//    } else {
//        if ([jsonArray isKindOfClass:[NSDictionary class]]) {
//            NSDictionary *itemData = (NSDictionary*)jsonArray;
//            
//            Item *item = [Item itemFromDictionary:itemData];
//            
//            [self.appdelegate addItem:item];
//            
//        } else {
//            NSLog(@"NotDict: %@", [jsonArray class]);
//        }
//    }
//    
////    ...
//}
//
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
//    NSLog(@"finished!");
////    ...
//}
//
//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
//    NSLog(@"error: %@", error);
////    ...
//}
//
//
//
//
//
//
//
//
////// property declaration
////public $base_url;
////public $connection;
////// Constructor
////public function WhiplashApi($api_key, $api_version='', $test=false) {
////    if ($test == true) {
////        $this->base_url = 'http://testing.whiplashmerch.com/api/';
////    } else {
////        $this->base_url = 'https://www.whiplashmerch.com/api/';
////    }
////    
////    $ch = curl_init();
////    // Set headers
////    $headers = array('Content-type: application/json', 'Accept: application/json', "X-API-KEY: $api_key");
////    if ($api_version != '') {
////        array_push($headers, "X-API-VERSION: $api_version");
////    }
////    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
////    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
////    $this->connection = $ch;
////}
//
//
//
////// Basic REST functions
////public function get($path, $params=array()) {
////    $json_url = $this->base_url . $path;
////    $json_url .= '?' . http_build_query($params);
////    $ch = $this->connection;
////    curl_setopt($ch, CURLOPT_URL, $json_url);
////    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
////    $result =  curl_exec($ch); // Getting jSON result string
////    $out = json_decode($result); // Decode the result
////    return $out;
////}
////
////public function post($path, $params=array()) {
////    $json_url = $this->base_url . $path;
////    $ch = $this->connection;
////    curl_setopt($ch, CURLOPT_URL, $json_url);
////    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
////    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
////    $result =  curl_exec($ch); // Getting jSON result string
////    $out = json_decode($result); // Decode the result
////    return $out;
////}
////
////public function put($path, $params=array()) {
////    $json_url = $this->base_url . $path;
////    $ch = $this->connection;
////    curl_setopt($ch, CURLOPT_URL, $json_url);
////    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
////    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
////    $result =  curl_exec($ch); // Getting jSON result string
////    $out = json_decode($result); // Decode the result
////    return $out;
////}
////
////public function delete($path, $params=array()) {
////    $json_url = $this->base_url . $path;
////    $ch = $this->connection;
////    curl_setopt($ch, CURLOPT_URL, $json_url);
////    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
////    $result =  curl_exec($ch); // Getting jSON result string
////    $out = json_decode($result); // Decode the result
////    return $out;
////}
////            
/////** Item functions **/
////public function get_items($params=array()) {
////    return $this->get('items', $params);
////}
////
////public function get_item($id) {
////    return $this->get('items/'.$id);
////}
////
////public function get_items_by_sku($sku, $params=array()) {
////    return $this->get('items/sku/'.$sku, $params);
////}
////
////public function get_item_by_originator($id) {
////    return $this->get('items/originator/'.$id);
////}
////
////// This requires a valid ID
////public function create_item($params=array()) {
////    $p = array();
////    if (!array_key_exists('item', $params)) {
////        $p['item'] = $params;
////    } else {
////        $p = $params;
////    }
////    return $this->post('items', $p);
////}
////
////// This requires a valid ID
////public function update_item($id, $params=array()) {
////    $p = array();
////    if (!array_key_exists('item', $params)) {
////        $p['item'] = $params;
////    } else {
////        $p = $params;
////    }
////    return $this->put('items/'.$id, $p);
////}
////
////// This requires a valid ID
////public function delete_item($id) {
////    return $this->delete('items/'.$id);
////}
////
////
/////** Order functions **/
////public function get_orders($params=array()) {
////    return $this->get('orders', $params);
////}
////
////public function get_order($id) {
////    return $this->get('orders/'.$id);
////}
////
////public function get_order_by_originator($id) {
////    return $this->get('orders/originator/'.$id);
////}
////
////public function get_order_by_status($status) {
////    return $this->get('orders/status/'.$status);
////}
////
////// This requires a valid ID
////public function create_order($params=array()) {
////    $p = array();
////    if ( !array_key_exists('order', $params)){
////        $p['order'] = $params;
////    } else {
////        $p = $params;
////    }
////    if ( array_key_exists('order', $p) ){
////        if (array_key_exists('order_items', $p['order'])) {
////            $p['order']['order_items_attributes'] = $p['order']['order_items'];
////            unset($p['order']['order_items']);
////        }
////    }
////    return $this->post('orders', $p);
////}
////
////// This requires a valid ID
////public function update_order($id, $params=array()) {
////    $p = array();
////    if ( !array_key_exists('order', $params) ){
////        $p['order'] = $params;
////    } else {
////        $p = $params;
////    }
////    return $this->put('orders/'.$id, $p);
////}
////
////// This requires a valid ID
////public function delete_order($id) {
////    return $this->delete('orders/'.$id);
////}
////
////
/////** OrderItem functions **/
////public function get_order_item($id) {
////    return $this->get('order_items/'.$id);
////}
////
////public function get_order_item_by_originator($id) {
////    return $this->get('order_items/originator/'.$id);
////}
////
////// This requires a valid ID
////public function create_order_item($params=array()) {
////    $p = array();
////    if (!array_key_exists('order_item', $params)) {
////        $p['order_item'] = $params;
////    } else {
////        $p = $params;
////    }
////    return $this->post('order_items', $p);
////}
////
////// This requires a valid ID
////public function update_order_item($id, $params=array()) {
////    $p = array();
////    if (!array_key_exists('order_item', $params)) {
////        $p['order_item'] = $params;
////    } else {
////        $p = $params;
////    }
////    return $this->put('order_items/'.$id, $p);
////}
////
////// This requires a valid ID
////public function delete_order_item($id) {
////    return $this->delete('order_items/'.$id);
////}
//@end
